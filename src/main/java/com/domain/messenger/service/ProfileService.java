package com.domain.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.domain.messenger.database.DatabaseClass;
import com.domain.messenger.model.Profile;

public class ProfileService {

	private static Map<String, Profile> profiles = DatabaseClass.getProfiles();

	public ProfileService() {
		profiles.put("pawan", new Profile(1L, "pawan", "Pawan", "Dwivedi"));
	}

	public List<Profile> getAllProfiles() {
		return new ArrayList<>(profiles.values());
	}

	public Profile getProfile(String profileName) {
		return profiles.get(profileName);
	}

	public Profile addProfile(Profile profile) {
		profile.setId(profiles.size() + 1);
		return profiles.put(profile.getProfileName(), profile);
	}

	public Profile updateProfile(Profile profile) {
		if (profile.getProfileName().isEmpty())
			return null;
		return profiles.put(profile.getProfileName(), profile);
	}

	public Profile removeProfile(String profileName) {
		return profiles.remove(profileName);
	}

}

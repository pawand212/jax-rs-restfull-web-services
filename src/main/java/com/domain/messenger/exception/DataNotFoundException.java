package com.domain.messenger.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2340147650727433134L;

	public DataNotFoundException(String message) {
		super(message);
	}
}
